package main

import (
	"fmt"
	"log"

	"github.com/fatih/color"
	"gitlab.com/emirot.nolan/go-test-coverage/mymath"
	"gopkg.in/yaml.v3"
)

var data = `
a: Easy!
b:
  c: 2
  d: [3, 4]
`

type T struct {
	A string
	B struct {
		RenamedC int   `yaml:"c"`
		D        []int `yaml:",flow"`
	}
}

func yam() {
	t := T{}
	err := yaml.Unmarshal([]byte(data), &t)
	if err != nil {
		log.Fatalf("error: %v", err)
	}
	fmt.Printf("--- t:\n%v\n\n", t)
}

func main() {
	fmt.Println("start")
	b := mymath.Sub(1, 3)
	fmt.Println(b)
	color.Cyan("Prints text in cyan.")
	yam()
}

