import (
	"fmt"
	"testing"
	"gopkg.in/yaml.v3"
)

func TestYamlUnmarshal(t *testing.T) {
	input := []byte(
`deploy_other_infra:
  stage: 'Prod Post Deploy'
  trigger:
   include:
     - project: 'my/gitlab-templates/nolna'
	   ref: 'v1'
	   file: 'pipeline.yml'`)

	var p TriggerGitlabCiYaml
	if err := yaml.Unmarshal(input, &p); err != nil {
		fmt.Printf("here:%+v", p)
		t.Errorf("Failed unmarshalling the struct %#v", err)

	}
	fmt.Printf("RES:%#v", p)

}

