module gitlab.com/emirot.nolan/go-test-coverage

go 1.16

require (
	github.com/fatih/color v1.10.0
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b
)

