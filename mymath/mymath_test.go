package mymath

import (
	"testing"
)

func TestSub(t *testing.T) {
	res := Sub(1, 3)
	if res != -2 {
		t.Fatalf("failed!")
	}
}

func TestMult(t *testing.T) {
	res := Mult(1, 3)
	if res != 3 {
		t.Fatalf("failed!")
	}
}

// func TestAdd(t *testing.T) {
// 	res := Add(1, 3)
// 	if res != 4 {
// 		t.Fatalf("failed!")
// 	}
// }

